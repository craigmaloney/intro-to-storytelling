all: intro_to_storytelling.tex intro_to_storytelling.pdf

intro_to_storytelling.tex : intro_to_storytelling.md metadata.yaml
	pandoc -s --template=Pandoc/templates/cs-6x9-pdf.latex -o $@ $^

intro_to_storytelling.pdf : intro_to_storytelling.md metadata.yaml
	pandoc -s --template=Pandoc/templates/cs-6x9-pdf.latex --pdf-engine=xelatex -o $@ $^

clean:
	rm *.pdf *.tex
