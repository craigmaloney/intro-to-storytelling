\tableofcontents{}

\newpage{}

# Introduction

This is a book dedicated to teaching you about Interactive Storytelling. By the end if this book you'll have enough of an idea on what Interactive Storytelling is that you'll be able to create your own Interactive Storyworlds. It does not purport to be a history of the art form, nor does it intend to to be a comprehensive survey of the field of Interactive Storytelling. There are plenty of other resources and materials available that will serve those purposes. The intention of this book is to provide a synthesis and restatement of the principles of Interactive Storytelling as set forth by Chris Crawford. This book will break down these concepts into practical ways you can use to build your own storyworlds. We'll start with basic concepts and build up to a framework that you can apply to your own storyworlds. Our main aim is to give you enough knowledge where you can apply this to your own creations. We may reference other interactive media to help clarify what Interactive Storytelling is but familiarity with these media is not required.

## Interactive

One key component of Interactive Storytelling is there in its name: Interactivity. The person playing the protagonist of the story is always interacting with the story. Nothing of consequence happens in the story without the protagonist setting it in motion. Sure, there may be things happening outside the protagonist's immediate horizon but the protagonist is the one driving the story. They make meaningful changes that influence the world they will be inhabiting at the end of the story. They are an active participant, not a passive tourist.

## Storytelling

The story is a series of events (known as encounters) in which the protagonist lives. These encounters are conveyed to the player taking on the protagonist's role. Most times this will be through the medium of text, though images may be used to help guide the player.These encounters are doled out to the player who then makes meaningful choices for the protagonist. The story is not an afterthought or something the player pieces together after-the-fact, but rather is the rich, connected environment that the protagonist inhabits.

Don't worry if these seem a bit confusing at first. You're only in the introductory chapter, and there's plenty of book left to explain these further.

## A few prerequisites

One thing that you must keep in mind when exploring Interactive Storytelling is that it requires a shift in perspective compared with media that you're currently familiar with. Interactive Storytelling shares some of the DNA with video games and Interactive Fiction but there are subtle differences between all of these media. Some games leave the story for the player to piece together into a coherent narrative (I moved into the room and hit the dragon with my sword). Others are a collection of cut-scenes that the player uncovers after certain actions are performed. Interactive Storytelling, by contrast, tells a complete story from start to finish. Like a good book you might need to play an Interactive Storyworld several times to fully understand what happened. Likewise, Interactive Storytelling is not the same as Interactive Fiction. Classic Interactive Fiction requires you the player to be as clever as the designer in figuring out various puzzles and traps. Interactive Storytelling relies on you inhabiting the role of the protagonist and filling their role and responsibilities in the Interactive Storyworld. There will be encounters that challenge and test you but they do so from the protagonist level, not the player level. Your protagonist may be asked to make difficult choices related to a situation rather than figure out a difficult puzzle. We'll cover this distinction more later on in the book. ^[And yes, some Interactive Fiction is moving away from puzzle-centric narrative and adopting the traits of Interactive Storytelling. The best part about taxonomies is that the real world resists taxonomies. (See also: metal music genres.)]

Another thing to keep in mind is that Interactive Storytelling relies heavily on skills that you have hopefully learned: reading, writing, and arithmetic. I trust that you're doing OK with the reading part, but the other two skills can be challenging for folks. Writing is fundamental for creating an Interactive Storyworld. It's through the designer's words that we build the encounters for the protagonist. If that terrifies you then you may want to practice writing short stories before tackling Interactive Storytelling. Not only will you be writing the encounters but you'll also be writing the responses that the protagonist and the responses of all of the characters in the world. That's a lot of writing. The other scary piece for many is the dreaded "math" word. Fortunately most of the arithmetic in Interactive Storytelling is simple checkbook math with a little bit of light algebra and statistics thrown in. If that still gives you heart palpitations keep in mind that you are no doubt in front of a computer that can do most of that work for you. The only thing you as a designer need to know is what calculations to make. The computer can take care of the messy details for you. Using your calculator / computer is encouraged here.

Interactive Storytelling is a fascinating and rich environment for using the computer to tell stories. As Chris Crawford puts it, this is "using the computer as a means of artistic expression". If that sounds enticing to you then I hope you'll join us on this journey.

## Some terminology

Throughout this book we'll use the word "player" to represent the person interacting with the storyworld and "protagonist" to refer to the player-character in the storyworld. This is to help clarify and simplify the complex relationships that can arise when interacting with a storyworld. Some folks believe in total immersion where player and protagonist become one. Others believe on a strict demarcation between the two. We simply ask that when we denote "player" or "protagonist" there is no ambiguity over which role we are referring.

# Basics of Interactive Storytelling

In this chapter we'll discuss some of the basics of Interactive Storytelling. We'll talk about encounters and characters; two fundamental building blocks of Interactive Storyworlds. We'll discuss how the protagonist interacts with the characters in this storyworld.

## Encounters

The first piece we'll discuss is the encounter. Encounters are where the protagonist gives their input into what is happening in the story. An encounter sets up the scene by describing not only where this encounter takes place but also the characters involved. It can also describe the stakes of this particular encounter.

Each encounter starts with a description of the situation that the protagonist is facing. It shows which characters are in the scene and shows what's at stake for the protagonist. Encounters should focus on dramatic situations. That means the situation should have clearly defined stakes that are meaningful choices for the protagonist. Meaningful choices can come about through the story. An example of this could be a moment where the protagonist must choose between sacrificing themselves for the good of others or allowing the others to come to harm because of their own selfish actions. Not all actions need to have this level of gravitas, however. There's usually nothing dramatic about choosing between coffee or tea for breakfast so this usually wouldn't warrant an encounter. We'd just assume that the protagonist made a choice and move on. But what if the protagonist was a life-long coffee drinker and his partner drank tea. The partner always chides the protagonist that he should relax and have a cup of tea. The protagonist staunchly refuses until one day when the partner is murdered because the protagonist overreacted to a situation. Then the choice between coffee or tea becomes meaningful: does the protagonist value their own routine and pig-headedness or does the protagonist decide to accept a new way of being? It's the stakes that unfurl in the storyworld that can make what could be a mundane, uninteresting choice into a dramatic decision with real consequences.

Remember: storyworlds should have dramatic, meaningful choices.

Let's explore this with a sample encounter. This is a sample encounter in an Interactive Storyworld about a pirate captain who is trying to prevent a mutiny on his ship. The characters in this encounter are the protagonist, Flint, the mutineer, and Errol, your first mate.

We start with the description:

> As you guide your ship out to intercept the S S MacGuffin (a merchant ship) you get an uneasy sense that the crew is not fully engaged in their tasks. "Speed up, men!", you shout, "We're losing our chance to intercept the MacGuffin!"
> Flint, one of your crew, looks up at you. "We're not going after these small merchant vessels anymore!", he exclaims. "Them's small potatoes compared to the bigger hauls."
> You look at Flint, annoyed. "if we go up against those bigger ships we'll all hang. We need to build up our fleet before we tackle those bigger ships."
> "Talk, talk talk" he says. "Don't you trust your crew? Aye, that you don't. Nor should you. We're living on scraps while you putter away what little life we have left."
> Other members of the crew start to gather around Flint. They look at you, wondering if your leadership is really warranted or if it's time for a new leader.
> Errol looks at you. It's pretty clear that Flint needs to be put in his place before things get even more ugly than they are now.

This sets up the stakes and the characters. Flint will need to be dealt with sharply and swiftly, but not at the expense of the loyalty of the crew. Errol seems undecided. Perhaps his allegiance is not to you, the captain. And perhaps you have made a bad call and should take more risks to get bigger and better rewards.

Here are the possible responses that are provided by the designer:

> 1) "Stand down, Flint, or I will have you swinging from the yard-arm by sundown."
> 2) "Flint, I need you all to trust my judgment. That is why I am commanding this ship."
> 3) "If we get the S S MacGuffin I'll forego my own share to everyone on this ship. Deal?"
> 4) "Bigger prizes need bigger armadas. Any experienced seaman knows that, right?"

In this example the designer has set up four different responses garnering different outcomes from the characters. These responses challenge Flint's assertion that you're not worthy to lead the crew. The first threatens violence against Flint himself (being taken out of commission). The second tries to reason with Flint by stating that you're the one in charge, not him. The third offers a bargain not only to Flint but to the rest of the crew by giving up your own share of the treasure. The fourth one explains your own goal of building a bigger armada to go after bigger prizes. They can be boiled down to different commands: "fear me", "trust me", "like me", and "respect me". The responses are direct and dramatic. At no point is there any ambiguity over what the protagonist is asking.

The reactions to these responses are also what drives the story and shows what the characters might be thinking. Characters are an important piece of the Interactive Storyworld. Interacting with the characters in this storyworld are the main point of the storyworld. Otherwise we could just have a ship of obedient robots taking orders and moving the ship from one score to another. Anticipating what the characters think and believe are a key ingredient in experiencing an interactive storyworld.

As you might expect Flint and the crew may have something to say in response to your own responses. Here are some examples:

For the first one: "Stand down, Flint, or I will have you swinging from the yard-arm by sundown." the response could be:

> The crew disperses from around Flint. Having no backing he grumbles and goes back to work. This is definitely not over.

The second one: "Flint, I need you all to trust my judgment. That is why I am commanding this ship." could elicit this response:

> Flint looks at you with a steely gaze. "You command as long as it's profitable. Don't you forget that."

The third response: "If we get the S S MacGuffin I'll forego my own share to everyone on this ship. Deal?" might have this response:

> This seems to please the crew, who go back to work. Flint looks annoyed at the loss of support but eventually relents and continues his tasks.

And the last response: "Bigger prizes need bigger armadas. Any experienced seaman knows that, right?" could provide this response:

> Flint glares at you as the crew disperses from around him. "Keep promising" he sneers.

Note that each of these responses deals with the problem at hand (Flint's rebellion) without deviating from your intended goal (going after the target). A good encounter doesn't have to introduce more complications into the story. We'll talk about branching later in this book but it's an important idea to keep in mind.

With this encounter done the designer can move on to other encounters. We'll continue with this example storyworld and show how to create your own encounters later in the book.

## Characters

Characters are the lifeblood of any storyworld. Characters are who the protagonist interacts with throughout the encounters in the storyworld. Certain characters may recur throughout different encounters and build relationships with the protagonist. Those relationships can either be positive or negative for the protagonist. Characters can be interesting, flamboyant, rude, mischievous, or cruel but they are never boring. They are constant companions to the protagonist, weaving interesting situations throughout the storyworld until its ultimate conclusion.

A character in an interactive storyworld is represented by several factors. On the surface level the character is described by the prose of the encounters. The creator writes the description of the character and any relevant parts that the protagonist and player may need to know about the character. The creator also endows these characters with variables that represent various aspects about the character. These variables keep track of various aspects of the character's own beliefs and personality that are relevant to the story. These variables become an important barometer of how the characters perceive the protagonist through their actions and how the world is changed by the action of the protagonist.

An interactive storyworld uses variables that are "bounded", meaning they have an upper and lower bound. We bound the variables from -1 to 1, with 0 being the mid-point.

\begin{figure}[H]
\caption{A Bounded Variable}
    \begin{pspicture}[showgrid=false](-1,-1)(1,1)
        \psaxes[yAxis=false,Dx=1]{-}(0,0)(-1,-1)(1,1)
    \end{pspicture}
\end{figure}

Interactive storyworlds use these bounded variables to track different personality and beliefs of a particular character. They take the form of the negative side being on the left and the positive side being on the right. An example of this is the variable name `bad_good`, which tracks the badness and goodness of the character. Another example could be the variable name `disloyal_loyal` to track the loyalty of a character to the protagonist. In the case of a romantic relationship the variable might be `abhor_adore` or `dislike_favor`.

Most times these variables start at 0 to show a neutral belief or personality. As the interactive storyworld progresses each interaction can nudge the value in a negative or a positive direction (or, if you prefer, backward or forward). We make these changes gradually, using small changes to nudge these values i the desired direction. An interaction between a couple where the protagonist tells their partner that they can't make their birthday party because of a work commitment might move a `ignored_attentive` variable in a negative direction by 0.03 (-0.03). However, if the protagonist tells their partner that they can't make their birthday party because they're going out with some other friends then that could have a more dramatic effect. `ignored_attentive` could move as much as -0.06 or even -0.1, depending on the storyworld. We might even have a situation where the partner knows that the protagonist is always ignoring their birthday and might not be as bothered, which might move the `ignored_attentive` just a hair at -0.01 or less. Again, different circumstances in the storyworld may have profound differences in how a character reacts to the protagonist.

<aside class="integer-vs-floating">
Some programmers might wonder why we use floating point numbers instead of integers for our bounded variables. Assume that we decided to use numbers from -100 to 100 instead. That gives us around 200 possibilities for what someone's personality might be. However with floating point variables we have far more gradations available to us. We're not limited to just 200 integers and have the full flexibility of whatever floating point math our interactive storyworld engine provides us.
</aside>

Let's return to our pirate-themed example above. We have introduced several obvious characters in this example. There's Flint, a pirate who challenges the protagonist's choice to go after the S S MacGuffin. There's also Errol, the protagonist's first-mate. Those are two named characters in this storyworld. What's less obvious are two other characters. There's the protagonist, the character that the player assumes the role of when they interact with the storyworld. And there's a final character in the crew. How can a group of people be a character in a storyworld? It's mostly a convenience for tracking variables related to the overall crew.

For characters like Flint, Errol and the overall crew we can create two variables: `disloyal_loyal` and `hate_like`. `disloyal_loyal` shows their loyalty to the protagonist while `hate_like` shows how much the character likes the protagonist. We might even have a variable for how much Flint, Errol, and the crew trusts the protagonist: `doubt_trust`. 

Each of the responses can have an effect on the characters in the encounter. These variables are hidden from the player and the protagonist but they can give a sense of what the characters are feeling and how this might play out in the larger interactive storyworld.

As a reminder here are the responses:

The first response was "Stand down, Flint, or I will have you swinging from the yard-arm by sundown". Flint is obviously not going to like this so his `disloyal_loyal` might be nudged down a bit. Let's add -0.06 to his current `disloyal_loyal` value. Errol might not like this either. His `disloyal_loyal` score might be tweaked by -0.01; not a lot but definitely trending downward. This might also shake up the crew a bit. Their `disloyal_loyal` may go down by -0.03. This response isn't going to win the protagonist much in the loyalty department. Nor will it make him many friends as Flint's `hate_like` probably takes an equal hit of -0.06. This doesn't affect Errol either way so we leave his `hate_like` alone. The crew might dislike this a bit but not too much. They adjust their `hate_like` to 0.01. However this does raise some doubt in everyone's mind. All of them have their `doubt_trust` adjusted by -0.03. This is definitely not a popular response. Threatening the crew of your ship with violence isn't going to solve anything.

The second response was "Flint, I need you all to trust my judgment. That is why I am commanding this ship". Flint isn't going to like this one bit so his `hate_like` will probably go down by -0.03. The crew and Errol will likely not be swayed by this response so their `hate_like` is unaffected. Flint's `disloyal_loyal` and `doubt_trust` will also likely take a hit but it won't be quite as bad as feeling like he'll be executed for showing what he considers a reasonable complaint. Both of those go down -0.02. The crew and Errol are not affected by this so their variables remain the same.

The third response was "If we get the S S MacGuffin I'll forego my own share to everyone on this ship. Deal?". Flint won't like this one either because it undermines his complaint completely. His `hate_like` will likely go down by -0.03, as will his `doubt_trust`. His `disloyal_loyal` might even go down by -0.02. Errol might even be surprised by this and have his `doubt_trust` go down by -0.01. The crew on the other hand might have their `hate_like` go up by 0.03 and have a small bump to their `disloyal_loyal` by 0.01.

The last response was "Bigger prizes need bigger armadas. Any experienced seaman knows that, right?". Flint will definitely take this as an insult and have his `hate_like` go down by -0.04. It's not quite the same as having someone tell you they'll execute you but it is a definite blow to the ego. His `disloyal_loyal` will also take a hit by -0.02, as will his `doubt_trust`. The crew and Errol might like this little jab and have their `hate_like`, `doubt_trust` and `disloyal_loyal` go up by 0.01. After all, this means you believe that their hard work will pay off and they might believe it too.

Note that there are no "perfect" choices in this. Flint is going to be annoyed with the protagonist no matter which response is chosen. And depending on the response the crew and Errol might also join Flint in wondering if the protagonist is worthy of captaining the ship. But this also highlights the subtle nature of the encounter. Words were exchanged, feelings were adjusted, and everyone moves about their business. Flint didn't get a "chastised" flag that he carts around; he just adjusts his perspective of you. The crew doesn't suddenly get a "we love the captain" flag; they subtly adjust their thinking based in your response. This highlights the power of an interactive storyworld. All of these encounters subtly tweak these characters by the protagonist's actions and deeds.

## Summary

We'll revisit these variables when we talk about designing encounters and storyworlds. For now just know the following:

* Encounters are where the protagonist interacts with the storyworld.
* Each encounter presents meaningful choices for the protagonist.
* Characters attitudes and personalities can be adjusted by the choices of the protagonist.
* Characters may have different aspects of their personality changed by the responses.
* There doesn't have to be a "perfect" response. Not all situations can be "won".

# Creating a story world

Now that we've seen some examples of encounters and characters in an interactive storyworld we can use these pieces to create our own storyworlds. We won't focus on specific tools in this chapter; rather we'll discuss general principles for creating interactive storyworlds. We'll also discuss what you'll need to consider in creating your own storyworlds and how to keep your focus while creating an interactive storyworld.

## What is your story about?

The first question to ask is "what is your story about?" This seems like a simple question but it can bring about clarity not only for why you are writing this interactive storyworld but also why anyone would make the time investment to play it. For Chris Crawford's "[Le Morte D'Arthur](https://www.erasmatazz.com/LeMorteDArthur.html)" the story is about living an interesting life that folks will continue to talk about for centuries. In Chris Crawford and Sasha Fenn's romantic comedy "[Wormsnot](https://www.emptiestvoid.com/encounter_system/wormsnot2023Dec29.html)" the story is about newfound love and whether one can grab hold of it before it disappears. In my own short storyworld "Decluttering" it's about the conversation one has while emptying a box and having a personal conversation about what to do with it all.

If you're stuck for what to write you might look at some of the works of great fiction to get some ideas. Books like "Pride and Prejudice", "Moby Dick", or "Huckleberry Finn" can be fertile areas for ideas and exploration (and they're all in the public domain so you should be able to find them online with little difficulty).

The main thing you should focus on when thinking about what your interactive storyworld is about is what interesting choices are available for the player to make. Interesting choices doesn't mean that the protagonist can go anywhere in the storyworld. Rather it means that the protagonist is challenged and tested with their beliefs. In a story like "Pride and Prejudice" the protagonists spend a lot of time sitting and making conversation. It's when you read into those conversations that you realize the social combat and verbal dexterity being put into play where saying the wrong thing could lead to one being socially ostracized in an era where social status means everything. There is dramatic tension and interesting choices to be had in these types of situations.

If you're still hard-pressed for an idea don't sweat it. Just give yourself some space to think about this and mull them around in your head. Alternatively, I recommend checking out "[The Thirty Six Dramatic Situations](https://archive.org/details/thirtysixdramati00poltuoft)" by Geoerges Polti and "[Hillfolk](https://pelgranepress.com/product/hillfolk/)" by Robin D. Laws et al From Pelgrane Press to give yourself some ideas.

For our pirate-themed example our over-all story is about sticking to your guns despite the odds. Throughout this story the protagonist will be tested both by the situations they face as well as their crew. It'll be tough for our protagonist but if they're smart and clever they'll be able to make it through with their skin intact and riches at the ready.

## What does the protagonist do?

FIXME: Need to make this more clear.

Once you have the overall theme of the story you'll want to make an outline of what situations the protagonist will be placed in. Get as detailed as you can with this. You'll want to think about situations you want the protagonist to face, the characters they'll encounter, and how this will all fit together in the larger story. You'll want to write down these situations and think about how they might be transformed into encounters. Cast your net wide in order to find these. You won't necessarily use all of them but you'll at least have them. Think about any dramatic twists and turns you might have the characters face as they navigate the storyworld and how that could provide dramatic tension and difficult choices. We'll also want to think about the story from different levels from the encounter level to the overall story. Don't be afraid to pull back and think about the story as a whole and determine if the encounters support that story.

This doesn't need to be an actual outline with different sections, sub-sections, and sub-sub-sections. You can make an outline of a storyworld on index cards or text files in a specialized editor. Whatever helps you get the ideas out and can be organized later will be fine. Play with this and allow yourself to explore.

You'll also want to figure out the order in which the encounters will be presented to the protagonist. This is more art than science. One approach is to use the [story beats](https://storybeats.io/) methodology by Robin D. Laws, which are explained in great detail in the books [Beating the Story](https://gameplaywright.net/books/beating-the-story/) or [Hamlet's Hit Points](https://gameplaywright.net/books/hamlets-hit-points/), both by Robin D. Laws. Story Beats can help show the flow of the story and can help you plan out dramatic moments (encounters) for the protagonists.

For our pirate-themed example we could have an outline that has the various situations we want to place our protagonist in. We might have several challenges to their authority, instances where they are negotiating over the spoils with the crew, an instance of betrayal, and merging several disparate crews together into a cohesive whole. For each of these we'll want to drill down into what characters are present and what we'll want to highlight about the protagonist. In the encounters where the protagonist is trying to merge two crews together we may have encounters highlighting the conflict between the crews and various crew members that are not happy about the situation. We may also have encounters where we need to take crew members aside to smooth things over. As we drill down even further we might think about other encounters that might arise and the decisions the protagonist will need to make. 

FIXME: (Insert a drawn example?)

## Write each encounter, rinse and repeat

Once you have the underlying story outlined you will want to start writing the encounters for the storyworld. Some folks prefer to start from the beginning and work their way to the end of the storyworld. Others may prefer a more scattershot approach; writing encounters from different places in the story until they all converge. Whichever method you choose the main goal is to write. Start by describing the scene: what does this place look like? What are the smells? What's the temperature? Describe the scene so that the player and the protagonist get a sense of where they are. What characters are present? What's the protagonist's relationship to the characters? What are the characters doing in the scene? Give any relevant details to the player so they can make informed choices about what the protagonist should be doing in this moment.

Here's how an encounter for our pirate-themed story might unfold:

> Darkness falls over the ship, and with it the cold damp air settles in. The moon is full tonight so much of the crew is awake doing their work by night rather than in the sweltering heat of day. The water is too calm with the wind barely stirring any waves. The night watch can see for miles in all directions. You tell the crew to move the boat closer to shore so you're not as conspicuous on the water.
> Suddenly the night watch signals that they see a ship off in the distance. It is brightly lit and reflects off of the calm waters. From the size it looks like a warship. What is it doing out this way? Did they spot us?
> You motion to the rest of the crew to keep still. Perhaps they haven't seen you yet. You motion over to Errol to get his opinion.
> "We should move closer to shore and use the trees over there to hide the ship from the warship" Errol says. "We're no match for that size of a ship".
> You nod. But there's something about that ship that is intriguing to you. Warships don't usually come out this way. And it doesn't appear that they've seen you yet. They're too brightly lit to see your dark ship, but that full moon isn't doing your ship or your crew any favors.

In this example we set the scene. We give the player information about the temperature, the time, and who is in the scene. We set up the dilemma for the player / protagonist to decide on: whether to hide from the warship or investigate further. We dangle a dramatic carrot in front of the player / protagonist: hide and hope they didn't see you or get closer and investigate. 

## Provide interesting responses

Each of your encounters should have meaningful, interesting, and dramatic choices for the player / protagonist to choose from. Most encounters have between one to four responses to choose from. Three choices tends to be the sweet spot for most encounters. More than four can be too much for the player. Too many choices can muddy the impact of each choice so that each choice is harder to differentiate from each other. If an encounter is a transitional scene then one choice is sufficient.

In the example above we could choose to have one, two, or three choices. Let's explore what that might look like.

If we have one choice then that choice could be:

> 1) "Move the ship closer to shore. We don't want to risk being seen by that ship."

This is a fine choice but it pulls away from the dramatic set-up for the scene. We went through the trouble of setting up a dramatic situation for the player / protagonist but we pulled the meaningful choice away from them. So making this a one-choice scene loses the dramatic power of the scene.

Let's try two different responses:

> 1) "Move the ship closer to shore. We don't want to risk being seen by that ship."
> 2) "Let's get a little closer without being seen. Something's not right about that ship and we should investigate."

This offers more dramatic tension. We still have the original choice (camouflage the ship) but we also have a choice for getting closer to the warship (stay out of sight but move a little closer. We've given the player / protagonist two dramatically distinct choices.

How about three choices? Let's try that:

> 1) "Move the ship closer to shore. We don't want to risk being seen by that ship."
> 2) "Let's get a little closer without being seen. Something's not right about that ship and we should investigate."
> 3) "They don't see us. When they pass by us we should pull up behind them."

This third choice (sneak up behind them) is similar to the second choice (investigate) but offers a subtle difference. Where the second choice feels more tepid (investigate from a distance) the third choice offers a more visceral investigation possibility. Depending on the story this might be a prudent move or completely reckless.

Could we squeeze in a fourth option? Let's give it a shot:

> 1) "Move the ship closer to shore. We don't want to risk being seen by that ship."
> 2) "Let's get a little closer without being seen. Something's not right about that ship and we should investigate."
> 3) "They don't see us. When they pass by us we should pull up behind them."
> 4) "Errol, I trust your judgment on this. Do what you feel is right."

Ah yes, the old delegate responsibility trick. Not as direct as the other ones but allows you to place blame later on. This choice isn't as dramatic as the previous three but it does give some insight into the player / protagonist and how they deal with conflict. If this storyworld is more about leadership then this choice shows that the player / protagonist is more willing to step away from directly influencing the action and prefers a more passive role. Some might even call it a bad choice but sometimes our true personalities are shown not by our noble choices but in cowardice and delegation. And perhaps the designer believes that the real goal of leadership is letting your subordinates make interesting choices. Maybe this is a good choice after all? It depends on the view of the designer and the intention of the storyworld.

You can see that as we add choices we define more about our storyworld and the individual encounters. This is key in the design of a storyworld. What you add in responses defines what you the designer believe is important. Conversely what you leave out is also important. It's like the joke about making a statue about an elephant: all you need to do is remove the parts that aren't the elephant. The same is true for a storyworld: what choices you leave in is as important as the choices you leave out.

## React appropriately

One advantage of interactive storytelling is that the characters and the world can react to your choices. Characters can be enthralled, enraptured, or enraged with your decisions. As we saw in the earlier example between the protagonist and Flint there were several variables that were updated based on your responses. This allows for both subtle and profound shifts in the story. In a romantic story the protagonist might have whole responses removed from the story because of certain responses. Consider an encounter where the protagonist is introduced to the cat of one of the characters:

> Matilda jumps up onto your lap. Or, rather, Matilda puts out her front paws and tries to scramble her fluffy butt up into your lap. You can tell Matilda hasn't been declawed or had her nails trimmed in a while because with each scrambling paw she digs into your legs. Finally Matilda makes it up into your lap after what seems like an eternity. She looks at you expectantly as though she wants you to pet her.

And here are the responses:

> 1) You shuffle the cat off your lap. You have more important matters to attend to.
> 2) You give Matilda a few head pats and then shove Matilda off your lap.
> 3) You sit patiently petting Matilda until the cat is satisfied you have petted her long enough.

With the first response you might move your romantic interest's `annoyed_happy` well into the negative range. The second response might annoy them as well but not as much. The third response, however, might move your romantic interest's `annoyed_happy` in a more positive direction. Keep that in mind because you might later have an encounter where the romantic interest's `annoyed_happy` variable is tested to see if they are happy enough to offer another date. If your interaction with Matilda is negative that might pull their `annoyed_happy` variable to where the only option the romantic interest offers is that they might call you, eventually.

It's best to treat these characters as real people with real ambitions and goals. This is a radical departure from some games where the characters are treated as information vending machines. These variables can give an added depth to the characters that can have surprisingly complex reactions to responses.

We'll talk more about characters in the next chapter.

## Set up the next encounter

Many of your encounters will link to other encounters. The next encounter that a protagonist moves to in the story will be based on the response they made in the current encounter. Many times the response will only influence the characters in the scene and the protagonist will move to the same encounter, regardless of response. If the protagonist is having a conversation with another character most of the time you'll send the protagonist to the same encounter. An example of this could be:

> 1) You look to the waitress. "I'll have the special" and hand her the menu.
> 2) You give the waitress a smile. "I'm going to go for the burger and fries" and hand her the menu.
> 3) You heave a little sigh to the waitress. "I'm going to be responsible today. Please give me the healthy platter" you say, and hand her the menu.

In each of these cases the next encounter could be something about continuing your conversation with the person you're talking to and your meal arriving. Branching to different encounters is unnecessary. You _could_ make different encounters for the special, the burger, and the healthy platter but the less branching you do in your storyworld the less loose-ends you'll need to tidy up.

Branching adds complexity to any storyworld. The less branching you do in your storyworld the more time you'll have for writing encounters and creating better characters. Consider the following storyworld. Let's say that in the first four encounters you decide to provide four different branches for the story. Encounter one has four branches. Each of those encounters has four branches. The third level of encounters also has four branches each, as does the fourth level of encounters. By the time you are four encounters deep you'll have 256 branches to take care of. That's an awful lot of branches and you haven't even gotten too deep into the story. We propose a more "palm-tree" style branching system where the branches don't show up until the very end or at very specific points in the story which are then folded back into the main narrative.

"But, aren't branches required for interactivity?" you might ask? The interactivity is in the responses and how the characters react to those responses. Giving the protagonist choices that branch the story aren't indicative of interactivity. If you're giving your protagonist choices just to give them the feeling of being able to go anywhere in the story that's not necessarily a meaningful choice for them to take. Consider an investigative storyworld where the protagonist can go into the parlor, the kitchen, the dining area, or the library. Are these interesting choices? Hardly. There's no compelling reason to choose any of those rooms over the other rooms. And, since it's an investigation, the investigator will likely wind up in each of the rooms throughout the course of the storyworld. Again, it's better to focus on telling the story than focus on providing meaningless choices to give the illusion of interactivity.

Let's return to our pirate-themed example. In it we gave the following responses for the protagonist to choose from:

> 1) "Move the ship closer to shore. We don't want to risk being seen by that ship."
> 2) "Let's get a little closer without being seen. Something's not right about that ship and we should investigate."
> 3) "They don't see us. When they pass by us we should pull up behind them."
> 4) "Errol, I trust your judgment on this. Do what you feel is right."

At first glance you may think that these would require four different encounters. However we can use a few tricks to keep our branching down to a minimum.

In the first response we could have an encounter where the warship passes by, oblivious to the protagonist's ship:

> The brightly-lit warship passes by slowly, like rolling gloom. The ship doesn't pay you any mind. It's like you're not even there. "That's odd", you mutter quietly. Errol seems to agree. "We should investigate what's going on" you say. "Something's not right with that ship."

The second response gets a little more daring encounter:

> Your crew skillfully navigates the ship so that it stays within the shadows, just out of sight of the passing warship. The bright lights of the warship make this difficult but your crew is used to stealthy sailing. What you observe is quite odd. "Something's not right about that ship" you mutter to Errol. He agrees. "We should investigate what's going on".

And the third response is even more daring:

> Your crew manages to get uncomfortably close to the warship without being detected. The bright lights of the warship make this difficult but your crew is used to stealthy sailing. What you observe is quite odd. "Something's not right about that ship" you mutter to Errol. He agrees. "We should investigate what's going on".

And the fourth response shows Errol asserting his curious nature:

> Errol orders the crew to get closer to the passing ship without being detected. Your crew skillfully navigates the ship so that it stays within the shadows, just out of sight of the passing warship. The bright lights of the warship make this difficult but your crew is used to stealthy sailing. What you and Errol observe is quite odd. "Something's not right about that ship" you mutter to Errol. He agrees. "We should investigate what's going on".

All four encounters show a different aspect of the passing ship but each encounter makes it clear that investigating the ship is going to be a major component of the story. Each of these encounters can then link to an encounter where your crew gathers more intelligence about the warship and realizes whatever secrets might be held within. At no time do we allow the player / protagonist to derail the story. Instead we give them the choice about how they want to interact with the crew and how cautious they want to be.

In the movie "Star Wars Episode IV: A New Hope" Luke Skywalker complains to Uncle Owen that he wanted to get some power converters at Tosche Station. Uncle Owen tells him to do his chores first, which includes cleaning up their just-purchased droids. This leads to Luke having to find R2-D2, which leads to him finding Ben Kenobi. Had Luke gone to Tosche station Uncle Owen and Aunt Beru would have likely still perished from the Imperial Stormtrooper blasters but Luke wouldn't have met Ben and might have lost R2-D2 in the process. He still might have joined the Rebel Alliance but he wouldn't have learned about the force and would have found himself at the other-end of the movie ill-prepared for taking out the Death Star (not to mention being unable to rescue Princess Leia). What might seem like a reasonable choice in the story could have knock-on-effects that derail the story and require the designer to create a series of situations to get the story back on track.

Keep in mind this isn't trickery. We're not forcing a meaningless choice like a magician forcing a card on an unwitting rube. We're still giving meaningful choices. We're also carefully piecing out the story so that we keep things interesting for the player / protagonist while reducing the amount of book-keeping we have to do as designers to keep the story flowing. Every time we branch we're adding to the cascade of information that we need to track. The fewer branches we need to track the happier we'll be and the tighter our stories will be.

# Populating our Storyworld: more about characters

We've touched briefly on how to incorporate characters into our storyworld. In this chapter we'll explore the relationship of the characters with the protagonist and with the encounters. We'll look closer at the protagonist and their role in the storyworld. We'll demonstrate how characters can grow and influence the storyworld through these encounters and the reactions of the protagonist. We'll also work on making our characters more "real" and ways to make memorable characters.

## The protagonist is the most important character

The protagonist in the story has a privileged role in our storyworld. You might say that the entire world revolves around them. That's because it does. Every encounter in the storyworld includes the protagonist and the protagonist gets to react in ever encounter. That's not to say that the protagonist knows everything about what's going on in the storyworld. They aren't an omniscient deity; rather they're the character that directly influences the story and the characters in the storyworld.

## Personality variables

FIXME: Awkward

We've mentioned in previous chapters about [bounded variables](#characters) and how they relate to our characters. Remember that bounded variables are variables that are within the range of -1 to 1 inclusive, and can be any floating point number with a central point around 0. What we haven't mentioned yet is where to store these variables. One place to store them is with the protagonist character. In the Hearsay engine that is the character named "you", which is the protagonist character. This character acts as the global scoreboard for the personality traits that are important in the storyworld. We've mentioned different variables like `abhor_adore`,`dislike_favor`, and even `bad_good`, but there can be other variables as well. In a game where the religious piety of the protagonist is important to the story we could have `agnostic_pious` to track this. Religious acts would add to the `agnostic_pious` variable and irreligious acts would subtract from it. This variable is based on the actions of the protagonist, and by extension the player playing the protagonist. It's best to think of these variables as a barometer of how much of a particular quality a character has rather than think of them in terms of scoring. Saying that our protagonist's `agnostic_pious` is at .3 only means that they're more pious than any value lower than .3, and less than any higher value.

As a designer you'll want to determine what positive values and negative values of a bounded value mean. This is not only what it means personally for the protagonist but what it also means for the characters that interact with the protagonist. In the case of `agnostic_pious` a negative value may mean that the church social group will start excluding the protagonist from various meetings, or might hold an intervention. A positive value may mean that the church social will expect more of the protagonist to dedicate more of their life in the service of their deity. That could lead to an encounter where one of your rivals looks at you expectantly and says "You are going to bring a dish to pass at the upcoming potluck?". This could also lead to further encounters where the rival expects a higher class of dish than what you're capable of making. Again, this is dependent on the storyworld you're constructing and the relationships and encounters you wish to highlight.

A good rule of thumb is to keep the number of personality variables linked to a character down to around three. That seems to be a manageable number of variables without too much overlap. Four is OK but you may find some of the variables tend to overlap with each other. Five or more variables can become burdensome with certain variables only getting used once or twice in a storyworld. If you find yourself with variables that are only getting set once or twice it's best to eliminate them and find other existing variables that can take their place. An example of this happening to me was in a storyworld I was crafting about the rise of personal computing. I had a variable called `closed_open` that I wanted to use to determine if the protagonist believed more in closed systems (proprietary) vs. open systems (open source, open documentation, etc.). I thought this would be a clever thing to track. Eventually I found that variable didn't get used as much as I would have liked and I abandoned it. I folded that into another variable `war_fun` (for the adage "Business is War" that Jack Tramiel had vs. the adage from Atari's early days where "Business is Fun"). Did it map 100%? No, but the `war_fun` variable was the closest to my original meaning and could track other choices from the protagonist.

FIXME: Elaborate, and figure out of this is the best place for this or if there is another place in the manuscript this should go.

This also highlights another design consideration: ensure that each encounter affects one or more of the participating characters. Encounters that don't change the participating characters in some way may be encounters that can be left out. If you have an encounter where none of the characters update their personality variables then perhaps it's an encounter that can be left out altogether. Some encounters may be setting up later encounters or giving some narrative pipe to get where they need to go. That's OK. The ones you need to be aware of are the encounters where nothing really happens and the story would be tighter if it was left out.

## Relationships

Relationships can provide fertile ground for sowing drama into a storyworld. With these relationships we can set up conflicts, misunderstandings, and clashing world-views. We'll look at how to model these relationships between different characters and how to build in these conflicts to tell satisfying stories.

### Actual

FIXME: Clarify this in light of the new organization. This paragraph should reflect actual. Leave the perceived for the next section.

Many storytelling engines have the ability to track not only what the current character's personality is but also what they perceive other characters and the protagonist perceive and believe. It can be useful to note what a character believes about another character. For instance, if we were doing a storyworld about high-school romance we could track what the other characters believe about each other. Using the variable `annoyed_happy` we could endow each character with a list of the other characters and what they feel about the other characters. In the case of our protagonist we could have a list of the following characters: Alice, Bob, and Carol. Under there we could have the `annoyed_happy` to show how much our protagonist is annoyed with or likes the other characters. This could also be reflected in the other characters. Alice's character could have a list of the following characters: You, Bob, and Carol with her own version of `annoyed_happy` for each character. Bob could also have a list of characters: You, Alice, and Carol with his own `annoyed_happy` variable, and Carol could have a `annoyed_happy` list containing You, Alice, and Bob. This opens up the possibility of each character having their own feelings toward the other characters, including the protagonist.

### Perceived

FIXME: Have a diagram of what the `annoyed_happy` relationships would look like from the personal perspective

Where this gets interesting is at a second level: what do the characters perceive the other characters believe about them. ^[Some engines call these "pValues" and prefix the value with a p character. We'll call them perceived values for clarity.] A perceived value holds what the character perceives about another character. Let's say that Alice believes that Bob dislikes her. Her perceived `annoyed_happy` for Bob could be -0.3. Bob, on the other hand, may be indifferent to Alice. His actual `annoyed_happy` for her could be as low as -0.01. He may perceive that her `annoyed_happy` for him is near 0 as well. Alice may indeed be happy with Bob and have an actual `annoyed_happy` of 0.2. Perhaps Bob's indifference to Alice makes her think he hates her? It'll be up to the protagonist to sort this mess out.

FIXME: Have a diagram of Alice and Bob's `annoyed_happy` relationship.

### Circumferential

Meanwhile our protagonist may have a different world-view entirely. These are referred to as "circumferential" variables. What they track is how one character perceives another character believes. That sounds a little strange so let's break it down with an example.

Alice has a list of circumferential variables. These are a list of the other characters (Bob, Carol, and You). For each of these characters is a different list of characters. Under Alice's list for Bob that list would contain Alice, Carol, and You. Each of those would contain a bounded variable such as `annoyed_happy`. 

FIXME: Diagram this relationship

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/characters_circumferential.pdf}
\end{figure}

So, what's the point of this? ^[Other than making our data structures teachers very happy that we were paying attention in class.] This allows us to model what a character thinks another character believes. In the case of Alice we can answer the question "what does Alice think that Bob believes about Alice's `annoyed_happy`?" Or "what does Alice think that The Protagonist (you) believes about Carol's `annoyed_happy`?" In short, we can model complex relationships between the various characters. This gives our characters the ability to gossip about other characters and come up with accurate or wildly off-base assertions about the other characters. It allows us to model misunderstandings, hurt feelings, and other "Kremlinology" ^[Kremlinology is a term used during the cold war as a way to determine information from the Soviet Union. Folks would study seating charts, meeting attendance, and other indirect clues to garner information about secret processes. Basically, it's high school for governments.] between our characters. 

These might seem difficult to track but the benefits of tracking these variables can provide a very powerful way of modeling relationships and the mistaken beliefs that others might have. Remember, relationships can provide ample space for drama and drama can lead to interesting stories.
